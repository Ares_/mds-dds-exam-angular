FROM node:16.13.1-alpine3.15 as builder
WORKDIR /app
# ENV PATH /app/node_modules/.bin:$PATH
ENV URL_BACK="https://limitless-lake-48351.herokuapp.com/api/tutorials"
COPY package.json ./
COPY package-lock.json ./
RUN npm ci --silent
COPY . ./
RUN npm run build --prod

FROM nginx:stable-alpine
COPY --from=builder /app/dist/mds-dds-exam-angular /usr/share/nginx/html
ENTRYPOINT nginx -g 'daemon off;'
