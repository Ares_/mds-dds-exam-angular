# Docker cmd

To build : docker build . -t "deploytpfront"

To run : docker run --name frontend -d deploytpfront:latest

# Heroku
Deploy at : https://deploy-front1.herokuapp.com/tutorials

Deploy by dockerfile :

- git add .
- git commit -m "init"
- heroku stack:set container
- git push heroku main
